﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "iostream"

/*	TODO
 * poriesit sum lebo treba
 */

using namespace cv;
using namespace std;

struct notePos
{
    int dist_to_closest_line;
    int nr_of_line;
};
struct dista
{
    int dist_prev;
    int pos_y;
};
struct line_arr
{
    int size;
    dista *ciary;
};
struct crop_help
{
    int start;
    int stop;
    int y_value;
};
struct crop_image
{
    int width;
    int height;
    int ypos;
    int xpos;
    Mat image;
};
struct crop_image_source
{
    vector<crop_image> item;
    Mat image;
};

//funkcia, ktora urcuje poziciu noty, na zaklade jej vzdialenosti od najblizsiej ciary
int getNotePosition(int line_dist,notePos note )
{
    int pos=0;
    int note_position=999;
    int abs_dist = abs(note.dist_to_closest_line);
    float bin = float(float(abs_dist)/float(line_dist));
	if (bin< 1.0f/4.0f)
        pos=2;
	if (bin>=float(1.0f/4.0f)&&bin<=float (5.0f/6.0f))
    {
        pos=1;
    }
    if (bin>float(5.0f/6.0f)&&bin<=float(6.0f/5.0f))
        pos=3;
    if (bin>float(6.0f/5.0f))
        pos=4;
    if (pos==1)
    {
        if (note.nr_of_line==0)
        {
            if (note.dist_to_closest_line<0)
                note_position=11;
            else
                note_position=13;
        }
        if (note.nr_of_line==1)
        {
            if (note.dist_to_closest_line<0)
                note_position=9;
            else
                note_position=11;
        }
        if (note.nr_of_line==2)
        {
            if (note.dist_to_closest_line<0)
                note_position=7;
            else
                note_position=9;
        }
        if (note.nr_of_line==3)
        {
            if (note.dist_to_closest_line<0)
                note_position=5;
            else
                note_position=7;
        }
        if (note.nr_of_line==4)
        {
            if (note.dist_to_closest_line<0)
                note_position=3;
            else
                note_position=5;
        }
    }
    if (pos==2)
    {
        if (note.nr_of_line==0)
            note_position=12;
        if (note.nr_of_line==1)
            note_position=10;
        if (note.nr_of_line==2)
            note_position=8;
        if (note.nr_of_line==3)
            note_position=6;
        if (note.nr_of_line==4)
            note_position=4;
    }

    if (pos==3)
    {
        if (note.nr_of_line==0)
            cout<<"ERR"<<endl;
        if (note.nr_of_line==1)
            cout<<"ERR"<<endl;
        if (note.nr_of_line==2)
            cout<<"ERR"<<endl;
        if (note.nr_of_line==3)
            cout<<"ERR"<<endl;
        if (note.nr_of_line==4)
            note_position=2;
    }

    if (pos==4)
    {
        if (note.nr_of_line==0 && note.dist_to_closest_line<0)
            note_position=14;
        if (note.nr_of_line==1)
            cout<<"ERR"<<endl;
        if (note.nr_of_line==2)
            cout<<"ERR"<<endl;
        if (note.nr_of_line==3)
            cout<<"ERR"<<endl;
        if (note.nr_of_line==4 && note.dist_to_closest_line<0)
            note_position=1;
        else
            cout<<"ERR"<<endl;
    }
    return note_position;
}

//iba pomocna funkcia
line_arr shiftArray(int pos,int size, line_arr lines)
{
    for(int i=pos; i<size; i++)
    {
        if(i==size-1)
        {
            lines.ciary[i].pos_y=0;
            lines.ciary[i].dist_prev=0;
            lines.size--;
        }
        else
        {
            lines.ciary[i].pos_y=lines.ciary[i+1].pos_y;
            lines.ciary[i].dist_prev=lines.ciary[i+1].dist_prev;
        }
    }
    return lines;
}

//funkcia, ktora vracia vzdialenost od najblizsiej ciary
notePos distToClosestLine(int pos_y,line_arr lines)
{
    notePos note_position;
    note_position.dist_to_closest_line=99999;
    for(int i=0; i<lines.size; i++)
        if (abs(lines.ciary[i].pos_y-pos_y) <= abs(note_position.dist_to_closest_line))
        {
            note_position.dist_to_closest_line=lines.ciary[i].pos_y-pos_y;
            note_position.nr_of_line=i % 5;
        }
    return note_position;
}

Mat notePosition(line_arr lines,vector<Point> coordinates,Mat image)
{
    typename vector<Point>::iterator vi;
    notePos note_position;
    int j;
    int dist_between_lines=abs(lines.ciary[2].dist_prev);

    for (vi = coordinates.begin(); vi != coordinates.end(); vi++)
    {
        note_position = distToClosestLine(vi->y,lines);
        j=note_position.dist_to_closest_line;
        j=getNotePosition(dist_between_lines,note_position);
        putText(image,to_string(j),*vi,FONT_HERSHEY_COMPLEX_SMALL, 1.5, cvScalar(204,0,0), 1, CV_AA);
    }
    return image;
}

//pomocna funkcia na detekovanie najcastejsie vyskutujuceho sa prvku vo vektore
float mostCommonElement (vector<float> v)
{
    typename vector<float>::iterator vi;
    float max = 0;
    float most_common = -1;
    map<float,float> m;
    for (vi = v.begin(); vi != v.end(); vi++)
    {
        m[*vi]++;
        if (m[*vi] > max)
        {
            max = m[*vi];
            most_common = *vi;
        }
    }
    return most_common*(180/CV_PI)-90;
}

//funkcia co cisti priestor medzi notovymi osnovami
Mat cleanNoise (Mat image,line_arr pom)
{
    int dist_between_lines = pom.ciary[2].dist_prev;
    //reduce noise between lines
    for (int i=0; i<pom.size; i++)
    {
        if(pom.ciary[i].dist_prev>3*dist_between_lines )
            for (int j=pom.ciary[i-1].pos_y+25; j<=pom.ciary[i].pos_y-25; j++)
                for (int k=0; k<=image.cols; k++)
                    image.at<uchar>(j,k)=0;
        if(pom.ciary[i].dist_prev>3*dist_between_lines && (abs(pom.ciary[i+1].pos_y-pom.ciary[i].pos_y))>3*dist_between_lines && i<pom.size-1)
        {
            for (int j=pom.ciary[i-1].pos_y+25; j<=pom.ciary[i+1].pos_y-25; j++)
                for (int k=0; k<=image.cols; k++)
                    image.at<uchar>(j,k)=0;
            shiftArray(i,pom.size,pom);
        }
    }
    return image;
}

// detekcia "hrubych" ciar, ktore su nad osminovymi notami co idu za sebou.
vector<crop_help> findWhiteLines(vector<int> coordinates,Mat image)
{
    typename vector<int>::iterator vi;
    typename vector<crop_help>::iterator vii;
    threshold(image,image,0,255,THRESH_BINARY+THRESH_OTSU);
    vector<crop_help> line_coordinates;
    crop_help x;
    for (vi = coordinates.begin(); vi != coordinates.end(); vi++)
    {
        for ( int j = 1 ; j<image.cols; j++)
        {
            if (image.at<uchar>(*vi,j)==255 && image.at<uchar>(*vi,j-1) == 0 )
            {
                int p = *vi;
                x.start = j;
                x.y_value = p;
                for (int k=j; k<image.cols; k++)
                {
                    if (image.at<uchar>(*vi,k)==255 && image.at<uchar>(*vi,k+1)==0)
                    {
                        x.stop = k+1;
                        line_coordinates.push_back(x);
                        j=k+1;
                        break;
                    }
                }
            }

        }
    }
    return line_coordinates;
}
Mat rgbToBinary(Mat image1)
{
    cvtColor(image1,image1,COLOR_BGR2GRAY);
    threshold(~image1,image1,128,255,THRESH_BINARY);
    return image1;
}
//vycistenie miesta v obrazku so suradnicami x,y a urcenou vyskou a sirkou
Mat cleanSourceImage(Mat image,int startx,int starty,int height,int width)
{
    for (int i=startx; i<startx+width; i++)
        for(int j=starty; j<starty+height; j++)
            image.at<uchar>(j,i)=0;

    return image;
}

//vyseknutie osminovych not z obrazu aby nemylili template matching. Viem detekovat iba pokial idu osminove noty za sebou a maju horizontalnu ciaru.
vector<crop_image> cropImage(Mat image,line_arr pom, vector<crop_help> coordinates)
{
    vector<crop_image> noty;
    crop_image i;
    int dist_to_prev_line = pom.ciary[2].dist_prev;
    typename vector<crop_help>::iterator vi;
    for (vi = coordinates.begin(); vi != coordinates.end(); vi++)
    {
        if(!(vi->start-30<0) && !(vi->y_value+5*dist_to_prev_line>image.rows)) //overflow of image protection
        {
        i.xpos=vi->start-30;
        i.ypos=vi->y_value;
        i.height=5*dist_to_prev_line;
        i.width=vi->stop+30-i.xpos;
        i.image = Mat(image, cv::Rect(vi->start-30,vi->y_value,abs(vi->stop-vi->start)+40,5*dist_to_prev_line)).clone();
        cleanSourceImage(image,vi->start-30,vi->y_value-dist_to_prev_line,6*dist_to_prev_line,abs(vi->stop-vi->start)+60);
        noty.push_back(i);
        }
    }
    //imshow("cropped image",i.image);
    return noty;
}

// vymazanie vsetkeho pod poslednou a nad prvou notovou osnovou
Mat reduceNoise(Mat image,line_arr pom)
{
    int dist_between_lines = 1.5*pom.ciary[2].dist_prev;
    //remove everything over top line
    for (int i=(pom.ciary[0].pos_y-dist_between_lines); i>0; i--)
    {
        for(int j=0; j<image.cols; j++)
            image.at<uchar>(i,j)=0;
    }
    //remove everything under bottom line
    for (int i=image.rows-1; i>pom.ciary[pom.size-1].pos_y+dist_between_lines; i--)
    {
        for(int j=0; j<image.cols; j++)
            image.at<uchar>(i,j)=0;
    }
    return image;
}

//detekcia hrubych ciar cez hough line transform
crop_image_source getLines2(Mat inputImage1, Mat inputImage,Mat binarizedImage,line_arr pom1)
{
    Mat horizontal = binarizedImage.clone();


    float horizontalsize = float(horizontal.cols)/30.f;//float(horizontal.cols)/pom;
    Mat horizontalStructure = getStructuringElement(MORPH_RECT, Size(horizontalsize,1));
    Mat structure = getStructuringElement(MORPH_RECT,Size(5,5));
    medianBlur(horizontal,horizontal,3);
    erode(horizontal, horizontal, horizontalStructure);
    dilate(horizontal, horizontal, horizontalStructure);
    erode(horizontal, horizontal, structure);
    threshold(horizontal,horizontal,128,255,THRESH_BINARY);
    Mat test=horizontal.clone();
    Mat cdst;

    imshow("horiz",horizontal);

    cvtColor(horizontal,cdst,CV_GRAY2BGR);
    vector<Vec2f> lines; // will hold the results of the detection
    HoughLines(horizontal, lines, 1, CV_PI/180, float(horizontal.cols)/25.f, 0, 0 ); // runs the actual detectio
    vector<int> os_y;
    crop_image_source crimg;

    for( size_t i = 0; i < lines.size(); i++ )
    {
        float rho = lines[i][0], theta = lines[i][1];
        Point pt1, pt2;
        double a = cos(theta), b = sin(theta);
        double x0 = a*rho, y0 = b*rho;

        pt1.x = cvRound(x0 + 5000*(-b));
        pt1.y = cvRound(y0 + 5000*(a));
        pt2.x = cvRound(x0 - 5000*(-b));
        pt2.y = cvRound(y0 - 5000*(a));
        if((atan2(pt2.y - pt1.y, pt2.x - pt1.x) * 180.0 / CV_PI)==0)
        {
            cv::line( cdst, pt1, pt2, Scalar(0,0,255), 3, CV_AA);
            os_y.push_back(pt2.y);
        }
    }
    if (!os_y.empty())
    {
        vector<crop_help> vec = findWhiteLines(os_y,test);
        typename vector<crop_help>::iterator vi;

        for (vi = vec.begin(); vi != vec.end(); vi++)
            rectangle(inputImage1, Rect(vi->start-30,vi->y_value,abs(vi->stop-vi->start)+40,5*pom1.ciary[2].dist_prev), Scalar(204,0,0), 2);

        crimg.item = cropImage(inputImage,pom1,vec);
        crimg.image=~inputImage.clone();
    }
    return crimg;
}

//detekcia notovych osnov cez hough line transform
line_arr getLines(Mat binarizedImage)
{
    Mat horizontal = binarizedImage.clone();
    struct line_arr abc;

    horizontal = binarizedImage.clone();

    float horizontalsize = float(horizontal.cols)/20.f;//float(horizontal.cols)/pom;
    Mat horizontalStructure = getStructuringElement(MORPH_RECT, Size(horizontalsize,1));
    erode(horizontal, horizontal, horizontalStructure);
    dilate(horizontal, horizontal, horizontalStructure);
    imshow("horizontal",horizontal);

    Mat cdst;
    cvtColor(horizontal,cdst,CV_GRAY2BGR);
    vector<Vec2f> lines; // will hold the results of the detection
    HoughLines(horizontal, lines, 1, CV_PI/180, float(horizontal.cols)/2.0f, 0, 0 ); // runs the actual detection
    struct dista *ciary=new dista[lines.size()];
    int point_y[lines.size()];
    ciary[0].dist_prev=0;
    // draw lines
    Point pt1_prev;
    for( size_t i = 0; i < lines.size(); i++ )
    {
        float rho = lines[i][0], theta = lines[i][1];
        Point pt1, pt2;
        double a = cos(theta), b = sin(theta);
        double x0 = a*rho, y0 = b*rho;

        pt1_prev.y=pt1.y;
        pt1.x = cvRound(x0 + 5000*(-b));
        pt1.y = cvRound(y0 + 5000*(a));
        pt2.x = cvRound(x0 - 5000*(-b));
        pt2.y = cvRound(y0 - 5000*(a));
        point_y[i]=pt1.y;
        cv::line( cdst, pt1, pt2, Scalar(0,0,255), 3, CV_AA);
    }
    std::sort(point_y,point_y+lines.size());
    for (uint i=0; i<lines.size(); i++)
    {
        if(i>0)
            pt1_prev.y=point_y[i-1];
        ciary[i].pos_y=point_y[i];
        ciary[i].dist_prev=abs(point_y[i]-pt1_prev.y);
    }
    abc.size=lines.size();
    abc.ciary=ciary;
    imshow("sb",cdst);

    return abc;
}

// ziskanie not z obrazku
Mat getNotes(Mat binarizedImage,int dist_between_lines)
{
    Mat vertical=binarizedImage.clone();
    float verticalsize = (float(dist_between_lines)/3.0f);
    Mat verticalStructure= getStructuringElement(MORPH_RECT,Size(1,verticalsize));
    erode(vertical, vertical,verticalStructure,Point(-1,-1));
    dilate(vertical, vertical, verticalStructure,Point(-1,-1));
    imshow("vertical",vertical);
    return vertical;
}
Mat rotateImage (Mat image)
{
    vector<Vec2f> lines; // will hold the results of the detection
    HoughLines(image, lines, 1, CV_PI/180, image.cols/2.0f, 0, 0 ); // runs the actual detection
    vector<float> arr;
    for( size_t i = 0; i < lines.size(); i++ )
    {
        float theta = lines[i][1];
        arr.push_back(theta);
    }
    float line_angle = mostCommonElement(arr);
    Mat M = getRotationMatrix2D(Point (image.cols/2,image.rows/2),line_angle,1);
    warpAffine(image,image,M,Size(image.cols,image.rows));
    return image;
}

//template matching not
vector<Point> templateMatch(Mat inputImage,Mat image,Mat nota,line_arr pom,int typ_noty,int xpos,int ypos)
{
    Mat tmatch;
    float dist = abs(pom.ciary[3].pos_y-pom.ciary[4].pos_y);
    // pokus o resize templatu podla vzdialenosti riadkov od seba
    //funguje to celkom dobre avsak tazko detekovat notu s nozickou
    float notaSize=(float(dist)*4.0f*1.1f)/float(nota.rows);
    float notaSize1=(float(dist)*3.0f*1.1f)/float(nota.rows);
    float notaSize2=(float(dist)*2.0f*1.1f)/float(nota.rows);
    float notaSize3=(float(dist)*1.0f)/float(nota.rows);
    if(typ_noty==1 || typ_noty==2 || typ_noty ==5 || typ_noty == 6)
        resize(nota,nota,Size(),notaSize,notaSize);
    if(typ_noty==4)
        resize(nota,nota,Size(),notaSize2,notaSize2);
    if(typ_noty==3)
        resize(nota,nota,Size(),notaSize1,notaSize1);
    if(typ_noty==8)
    {
        resize(nota,nota,Size(),notaSize3,notaSize3);
    }

    nota=getNotes(nota,dist);
    //threshold(nota,nota,190,255,THRESH_BINARY);

    vector<Point> coordinates;
    double minVal;
    double maxVal;
    Point minLoc;
    Point maxLoc;
    Point matchLoc;
	double thresh;
    matchTemplate(image,nota,tmatch,CV_TM_CCOEFF_NORMED); // tento typ template matchingu funguje celkom dobre
    minMaxLoc(tmatch,&minVal,&maxVal,&minLoc,&maxLoc,Mat());
    matchLoc=maxLoc;
    image=~image;
    cout<<typ_noty<<endl;
    cout<<maxVal<<endl;
    if (maxVal>0.72f)
	{   if (maxVal<0.8f)
			thresh=0.9f * maxVal;
		if (maxVal>=0.8f && maxVal<0.9f)
			thresh=0.85f * maxVal;
		if (maxVal>0.9f)
			thresh=0.80f * maxVal;
        threshold(tmatch,tmatch,thresh,1.,THRESH_BINARY);
        Mat1b resb;
        tmatch.convertTo(resb, CV_8U, 255);
        vector<vector<Point>> contours;
        findContours(resb, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);
        for (uint i=0; i<contours.size(); ++i)
        {
            Mat1b mask(tmatch.rows, tmatch.cols, uchar(0));
            drawContours(mask, contours, i, Scalar(200), CV_FILLED);
            minMaxLoc(tmatch, NULL, &maxVal, NULL, &maxLoc, mask);
            if(typ_noty==1)
            {
                rectangle(inputImage, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(0,255,0), 2);
                rectangle(image, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(0,255,0), 2);
                coordinates.push_back(Point(maxLoc.x+nota.cols,maxLoc.y+nota.rows));
            }
            if(typ_noty==2)
            {
                rectangle(inputImage, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(0,255,255), 2);
                rectangle(image, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(0,255,255), 2);
                coordinates.push_back(Point(maxLoc.x+nota.cols,maxLoc.y+nota.rows));
            }

            if(typ_noty==3)
            {
                rectangle(inputImage, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(204,153,255), 2);
                rectangle(image, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(204,153,255), 2);
                coordinates.push_back(Point(maxLoc.x+nota.cols,maxLoc.y+nota.rows));
            }
            if(typ_noty==4)
            {
                rectangle(inputImage, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(255,255,255), 2);
                rectangle(image, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(255,255,255), 2);
                coordinates.push_back(Point(maxLoc.x+nota.cols,maxLoc.y+nota.rows));
            }
            /*if(typ_noty==4)
            {
            rectangle(inputImage, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(204,153,255), 2);
            rectangle(image, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(204,153,255), 2);
            coordinates.push_back(Point(maxLoc.x+nota.cols,maxLoc.y+nota.rows));
            }*/
            if(typ_noty==5)
            {
                rectangle(inputImage, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(0,255,0), 2);
                rectangle(image, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(0,255,0), 2);
                coordinates.push_back(Point(maxLoc.x-dist,maxLoc.y+dist));
            }
            if(typ_noty==6)
            {
                rectangle(inputImage, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(0,255,255), 2);
                rectangle(image, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(0,255,255), 2);
                coordinates.push_back(Point(maxLoc.x-dist,maxLoc.y+dist));
            }
            if(typ_noty==8)
            {
                rectangle(inputImage, Rect(maxLoc.x+xpos, maxLoc.y+ypos, nota.cols, nota.rows), Scalar(204,0,0), 2);
                rectangle(image, Rect(maxLoc.x, maxLoc.y, nota.cols, nota.rows), Scalar(204,0,0), 2);
                coordinates.push_back(Point(maxLoc.x+xpos+nota.cols,maxLoc.y+ypos+nota.rows));
            }
        }
    }

    return coordinates;
}

//funkcia na pridanie Gaussian noise prebrata z https://stackoverflow.com/questions/43931749/adding-gaussian-noise-in-image-opencv-and-c-and-then-denoised
// program je odolny voci slabsiemu gaussovemu sumu
Mat addGaussianNoise(Mat image)
{
    Mat result;
    result = image.clone();
    cv::Mat noise = Mat(image.size(),image.type());
    float m = (40);
    float s = (40);
    randn(noise,m,s);
    result+=noise;

    return result;
}
//funkcia na pridanie salt and pepper noise prebrata z https://stackoverflow.com/questions/14435632/impulse-gaussian-and-salt-and-pepper-noise-with-opencv
// program nieje odolny voci salt and pepper noise : teoreticky by to slo keby prejdem obrazok median filtrom , ale to by musel byt obrazok relativne velkeho rozlisenia, aby median filter nepokazil noty pre template matching
Mat addSaltAndPepperNoise(Mat img)
{
    Mat saltpepper_noise = Mat::zeros(img.rows, img.cols,CV_8U);
    randu(saltpepper_noise,0,255);

    Mat black = saltpepper_noise < 5;
    Mat white = saltpepper_noise > 250;

    Mat saltpepper_img = img.clone();
    saltpepper_img.setTo(255,white);
    saltpepper_img.setTo(0,black);

    return saltpepper_img;
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
	Mat inputImage = imread("../img/skakalpes.png");
    imshow("Input image",inputImage);
    Mat cela_nota = imread("../img/template/celaNota.jpg");
    Mat nota = imread("../img/template/nota21.png");
    Mat polovicna_nota = imread("../img/template/polovicnaNota.png");
    Mat osminova_nota1 = imread("../img/template/osminovaNota1.png");
    Mat pauza = imread("../img/template/pauza.png");
    cv::resize(inputImage,inputImage,Size(),0.9,0.9);
    vector<Point> contours;
    nota = rgbToBinary(nota);
    polovicna_nota = rgbToBinary(polovicna_nota);
    cela_nota = rgbToBinary(cela_nota);
    pauza = rgbToBinary(pauza);
    osminova_nota1 = rgbToBinary(osminova_nota1);

    Mat polovicna_nota_reverse = polovicna_nota.clone();
    Mat nota_reverse = nota.clone();
    Mat A = getRotationMatrix2D(Point (polovicna_nota_reverse.cols/2,polovicna_nota_reverse.rows/2),180,1);
    warpAffine(polovicna_nota_reverse,polovicna_nota_reverse,A,Size(polovicna_nota_reverse.cols,polovicna_nota_reverse.rows));
    Mat D = getRotationMatrix2D(Point (nota_reverse.cols/2,nota_reverse.rows/2),180,1);
    warpAffine(nota_reverse,nota_reverse,D,Size(nota_reverse.cols,nota_reverse.rows));


    //add gaussian noise
    //inputImage=addGaussianNoise(inputImage);
    //add salt and pepper noise
    //inputImage=addSaltAndPepperNoise(inputImage);
    //imshow("image with noise",inputImage);
    Mat rotatedImage = inputImage.clone();
    Mat image,image1;
    crop_image_source item;

    cvtColor(rotatedImage,rotatedImage,COLOR_BGR2GRAY);

    //moznost vyberu tresholdingu - otsu je lepsi na cisty obrazok a adaptivny gauss je lepsi voci salt and pepper sumu a tienu na obrazku
    threshold(~rotatedImage,rotatedImage,0,255,THRESH_BINARY+THRESH_OTSU);
    //adaptiveThreshold(~rotatedImage,rotatedImage,255,ADAPTIVE_THRESH_GAUSSIAN_C,THRESH_BINARY,21,-2);


    //umele rotovanie obrazku o 5 stupnov, aby sa ukazalo fungujuce narovnanie naspat
    Mat M = getRotationMatrix2D(Point (rotatedImage.cols/2,rotatedImage.rows/2),5,1);
    warpAffine(rotatedImage,rotatedImage,M,Size(rotatedImage.cols,rotatedImage.rows));
    //upravenie naspat
    image1 = rotateImage(rotatedImage);
    imshow("rotatedImage",rotatedImage);

    line_arr pom = getLines(image1);

    //clean noise
    image = reduceNoise(image1,pom);
    image = cleanNoise(image1,pom);
    imshow("Noiseless img",image);

    item = getLines2(inputImage,image,image1,pom);

    if(item.image.cols>0)
        image1= item.image;
    else
        image1=~image;

    int dist_between_lines=pom.ciary[4].dist_prev;
    // get notes
    threshold(image1,image1,0,255,THRESH_BINARY+THRESH_OTSU);
    image = getNotes(~image1,dist_between_lines);

    cvtColor(image1,image1,COLOR_GRAY2BGR);
    imshow("a",image);

    //template maching podla roznych templatov
    Mat img = image.clone();
    contours = templateMatch(image1,image,nota,pom,1,0,0);
    image = notePosition(pom,contours,image1);

    image=img.clone();
    Mat image2;
    contours = templateMatch(image1,image,polovicna_nota,pom,2,0,0);
    image =notePosition(pom,contours,image1);

    image=img.clone();
    contours = templateMatch(image1,image,pauza,pom,3,0,0);
    image = notePosition(pom,contours,image1);


    //image=img.clone();
    //contours = templateMatch(image1,image,cela_nota,pom,4,0,0);
    //image2 =notePosition(pom,contours,image1);

    image=img.clone();
    contours = templateMatch(image1,image,nota_reverse,pom,5,0,0);
    image2 =notePosition(pom,contours,image1);

    image=img.clone();
    contours = templateMatch(image1,image,polovicna_nota_reverse,pom,6,0,0);
    image2 =notePosition(pom,contours,image1);
    typename vector<crop_image>::iterator vi;
    for (vi = item.item.begin(); vi!=item.item.end(); vi++)
    {
        Mat image3 = Mat(inputImage, cv::Rect(vi->xpos,vi->ypos,vi->image.cols+30,vi->image.rows)).clone();
        image3.copyTo(image1(Rect(vi->xpos,vi->ypos,image3.cols,image3.rows)));
        cvtColor(image3,image3,COLOR_BGR2GRAY);
        image3=getNotes(~image3,dist_between_lines);
        contours = templateMatch(image3,image3,osminova_nota1,pom,8,vi->xpos,vi->ypos);
        image2 =notePosition(pom,contours,image1);
    }
    imshow("source", image1);

}
MainWindow::~MainWindow()
{
    delete ui;
}
